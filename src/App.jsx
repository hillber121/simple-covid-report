import { useContext } from "react";
import { Transition } from "react-transition-group";

import "./App.scss";
import { CountriesContext } from "./components/store/CountriesProvider";
import ListOfCountries from "./components/Country/ListOfCountries";
import Filter from "./components/Filter/Filter";
import Modal from "./components/Modal/Modal";
import CountryDetail from "./components/Country/CountryDetail";
import ToTop from "./components/ToTop/ToTop";

function App() {
  const { countryModalShow, closeCountryModal } = useContext(CountriesContext);
  // Transition configuration
  const duration = 200;
  const defaultStyles = {
    transition: `all ${duration}ms ease-in-out`,
    opacity: 0,
  };
  const slideDownStyles = {
    entering: { opacity: 1 },
    entered: { opacity: 1 },
    exiting: { opacity: 0, top: "40%" },
    exited: { opacity: 0, top: "40%" },
  };

  return (
    <div className="App">
      <Filter />
      <ListOfCountries />
      <Transition
        in={countryModalShow}
        timeout={duration}
        mountOnEnter
        unmountOnExit
      >
        {(state) => (
          <Modal
            onBackdropClick={() => closeCountryModal()}
            style={{
              ...defaultStyles,
              ...slideDownStyles[state],
            }}
          >
            <CountryDetail />
          </Modal>
        )}
      </Transition>
      <ToTop />
    </div>
  );
}

export default App;
