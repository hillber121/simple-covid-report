import { useState, useEffect, useContext } from "react";
import { Transition } from "react-transition-group";

import "./_filter.scss";
import { CountriesContext } from "../store/CountriesProvider";
import { ReactComponent as ArrowDIcon } from "./../../assets/icons/arrow_down.svg";

export default function Filter() {
  const { filterData, filterSelected, selectOptHandler } =
    useContext(CountriesContext);
  const [isDropdownClose, setIsDropdownClose] = useState(true);
  // If dropdown is opening, clicking outside or selecting other option will close it
  useEffect(() => {
    const closeDropdown = () => {
      setIsDropdownClose(true);
    };
    document.body.addEventListener("click", closeDropdown);
    return () => {
      document.body.removeEventListener("click", closeDropdown);
    };
  }, []);
  // Handle show and hide dropdown list
  const toggleDropdown = (event) => {
    event.stopPropagation();
    setIsDropdownClose((prev) => !prev);
  };
  // Dropdown classes
  const DropdownClasses = `Filter__dropdown ${
    !isDropdownClose ? "Filter__dropdown--active" : ""
  }`;
  // Transition configuration
  const duration = 200;
  const defaultStyles = {
    transition: `all ${duration}ms ease-in-out`,
    opacity: 0,
  };
  const slideDownStyles = {
    entering: { opacity: 1 },
    entered: { opacity: 1 },
    exiting: { opacity: 0 },
    exited: { opacity: 0 },
  };
  // Handle select
  const selectHandler = (event, opt) => {
    event.stopPropagation();
    selectOptHandler(opt);
    // const selected = event.target.innerText;
    // const selected = props.prices.find((price) => price.size === selectedSize);
    // props.onSelected(selected);
    setIsDropdownClose(true);
  };

  return (
    <div className="Filter">
      <h1>Countries affected by Covid-19</h1>
      <div className={DropdownClasses}>
        <div className="Filter__dropdown-default" onClick={toggleDropdown}>
          <p>{filterSelected.text}</p>
          <ArrowDIcon />
        </div>
        <Transition
          in={!isDropdownClose}
          timeout={duration}
          mountOnEnter
          unmountOnExit
        >
          {(state) => (
            <ul
              className="Filter__dropdown-opts"
              style={{
                ...defaultStyles,
                ...slideDownStyles[state],
              }}
            >
              {filterData &&
                filterData.map((filterOpt) => (
                  <li
                    key={filterOpt.value}
                    onClick={(event) => selectHandler(event, filterOpt)}
                  >
                    {filterOpt.text}
                  </li>
                ))}
            </ul>
          )}
        </Transition>
      </div>
    </div>
  );
}
