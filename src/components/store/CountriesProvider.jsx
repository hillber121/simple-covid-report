import { useState, createContext, useCallback, useMemo } from "react";

export const CountriesContext = createContext({});

export default function CountriesProvider({ children }) {
  const [isLoading, setIsLoading] = useState(false);
  const [err, setErr] = useState(null);

  // Fetch covid data
  const [countriesCovidData, setCountriesCovidData] = useState(null);
  const fetchCovidData = useCallback(async () => {
    setIsLoading(true);
    setErr(null);
    try {
      const response = await fetch("https://api.covid19api.com/summary");
      if (!response.ok) {
        throw new Error("Fetch covid data failed!");
      }
      const { Countries: countries } = await response.json();
      const loadedCountries = await [];
      for (let key in countries) {
        loadedCountries.push({
          country: countries[key].Country,
          countryCode: countries[key].CountryCode,
          totalConfirmed: countries[key].TotalConfirmed,
          totalDeaths: countries[key].TotalDeaths,
          totalRecovered: countries[key].TotalRecovered,
        });
      }
      setCountriesCovidData(loadedCountries);
    } catch (err) {
      setErr(err.message);
    }
    setIsLoading(false);
  }, []);

  // Filter handler
  const filterData = useMemo(
    () => [
      { value: "confirmed", text: "Highest confirmed" },
      { value: "deaths", text: "Highest deaths" },
      { value: "recovered", text: "Highest recovered" },
    ],
    []
  );
  const [filterSelected, setFilterSelected] = useState(filterData[0]);
  const selectOptHandler = (payload) => {
    setFilterSelected(payload);
  };

  // Display country detail handler
  const [countryModalShow, setCountryModalShow] = useState(false);
  const showCountryModal = () => {
    setCountryModalShow(true);
  };
  const closeCountryModal = () => {
    setCountryModalShow(false);
  };

  // Detail country information handler
  const [countryDetail, setCountryDetail] = useState(null);
  const [isModalLoading, setIsModalLoading] = useState(false);
  const [modalErr, setModalErr] = useState(null);

  const fetchDetailInfo = useCallback(async (countryCode) => {
    setIsModalLoading(true);
    setModalErr(null);
    try {
      const response = await fetch("https://restcountries.com/v3.1/all");
      if (!response.ok) {
        throw new Error("Fetch country information failed!");
      }
      const countryDetailInfoAll = await response.json();
      const countryDetailInfo = countryDetailInfoAll.find(
        (country) => countryCode === country.cca2
      );
      setCountryDetail({
        name: countryDetailInfo.name.official,
        flagUrl: countryDetailInfo.flags.svg,
        population: countryDetailInfo.population,
        capital: countryDetailInfo.capital,
        region: countryDetailInfo.region,
        subregion: countryDetailInfo.subregion,
      });
    } catch (err) {
      setModalErr(err.message);
    }
    setIsModalLoading(false);
  }, []);

  const countriesContext = {
    isLoading,
    err,
    countriesCovidData,
    fetchCovidData,
    filterData,
    filterSelected,
    selectOptHandler,
    countryModalShow,
    showCountryModal,
    closeCountryModal,
    fetchDetailInfo,
    countryDetail,
    isModalLoading,
    modalErr,
  };

  return (
    <CountriesContext.Provider value={countriesContext}>
      {children}
    </CountriesContext.Provider>
  );
}
