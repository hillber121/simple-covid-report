import { useState, useEffect } from "react";
import { animateScroll } from "react-scroll";

import "./_toTop.scss";
import { ReactComponent as ArrowTIcon } from "./../../assets/icons/arrow_top.svg";

export default function ToTop() {
  // Notice scroll
  const [isScrolled, setIsScrolled] = useState(window.scrollY !== 0);
  useEffect(() => {
    const handleScroll = () => {
      if (window.scrollY !== 0) {
        setIsScrolled(true);
      } else setIsScrolled(false);
    };
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);
  // Handle scroll to top
  const scrollToTopHandler = () => {
    animateScroll.scrollToTop({ duration: 300 });
  };
  return (
    isScrolled && (
      <div className="ToTop" onClick={scrollToTopHandler}>
        <ArrowTIcon />
      </div>
    )
  );
}
