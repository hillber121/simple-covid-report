import { useEffect, useContext } from "react";

import "./_countryDetail.scss";
import { CountriesContext } from "../store/CountriesProvider";
import Loading from "../Loading/Loading";

export default function CountryDetail() {
  const { fetchDetailInfo, countryDetail, isModalLoading, modalErr } =
    useContext(CountriesContext);
  useEffect(() => {
    fetchDetailInfo();
  }, [fetchDetailInfo]);
  let countryDetailComp;
  if (!countryDetail) {
    if (isModalLoading) {
      countryDetailComp = (
        <div className="CountryDetail__title">
          <Loading className="CountryDetail__title-loading" />
        </div>
      );
    } else if (modalErr) {
      countryDetailComp = (
        <div className="CountryDetail__title">
          <h1>{modalErr}</h1>
        </div>
      );
    }
  }
  if (countryDetail) {
    countryDetailComp = (
      <>
        <div className="CountryDetail__title">
          <h1>{countryDetail.name}</h1>
          <img src={countryDetail.flagUrl} alt="country flag" />
        </div>
        <p>Capital: {countryDetail.capital}</p>
        <p>Population: {countryDetail.population}</p>
        <p>Region: {countryDetail.region}</p>
        <p>Sub Region: {countryDetail.subregion}</p>
      </>
    );
  }

  return <div className="CountryDetail">{countryDetailComp}</div>;
}
