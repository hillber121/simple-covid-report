import { useContext } from "react";

import "./_country.scss";
import { CountriesContext } from "../store/CountriesProvider";

export default function Country({ countryData }) {
  const { showCountryModal, fetchDetailInfo } = useContext(CountriesContext);
  const showCountryDetail = () => {
    fetchDetailInfo(countryData.countryCode);
    showCountryModal();
  };
  return (
    <li className="Country" onClick={showCountryDetail}>
      <h1>{countryData.country}</h1>
      <p className="Country__confirmed">
        Total Confirmed: {countryData.totalConfirmed}
      </p>
      <p className="Country__deaths">Total Deaths: {countryData.totalDeaths}</p>
      <p className="Country__recovered">
        Total Recovered: {countryData.totalRecovered}
      </p>
    </li>
  );
}
