import { useEffect, useContext } from "react";

import "./_listOfCountries.scss";
import { CountriesContext } from "../store/CountriesProvider";
import Country from "./Country";
import Loading from "../Loading/Loading";

export default function ListOfCountries() {
  const countriesCtx = useContext(CountriesContext);
  const { fetchCovidData, countriesCovidData, isLoading, err, filterSelected } =
    countriesCtx;
  useEffect(() => {
    fetchCovidData();
  }, [fetchCovidData]);

  // Data display handler
  let countryComp;
  if (!countriesCovidData) {
    if (isLoading) {
      countryComp = <Loading />;
    } else if (err) {
      countryComp = <div className="ListOfCountries__err">{err}</div>;
    }
  }
  if (countriesCovidData) {
    if (countriesCovidData.length === 0) {
      countryComp = (
        <div className="ListOfCountries__err">Covid-19 data is catching...</div>
      );
    } else {
      countryComp = countriesCovidData
        .sort((countryDataA, countryDataB) => {
          let displayCountriesData = [];
          switch (filterSelected.value) {
            case "confirmed":
              displayCountriesData =
                countryDataB.totalConfirmed - countryDataA.totalConfirmed;
              break;
            case "deaths":
              displayCountriesData =
                countryDataB.totalDeaths - countryDataA.totalDeaths;
              break;
            case "recovered":
              displayCountriesData =
                countryDataB.totalRecovered - countryDataA.totalRecovered;
              break;
            default:
              displayCountriesData =
                countryDataB.totalConfirmed - countryDataA.totalConfirmed;
          }
          return displayCountriesData;
        })
        .map((countryData) => (
          <Country key={countryData.countryCode} countryData={countryData} />
        ));
    }
  }

  return <ul className="ListOfCountries">{countryComp}</ul>;
}
