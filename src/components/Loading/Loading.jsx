import "./_loading.scss";
import { ReactComponent as SpinnerIcon } from "./../../assets/icons/spinner.svg";

export default function Loading({ className }) {
  return <SpinnerIcon className={`Loading ${className ? className : ""}`} />;
}
